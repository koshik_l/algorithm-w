module Parser where

import Data.Char
import Control.Applicative
import AlgorithmW 

newtype Parser a = Parser { runParser :: String -> Maybe (a, String) }

satisfy :: (Char -> Bool) -> Parser Char
satisfy p = Parser f
  where 
    f [] = Nothing
    f (x:xs)
        | p x = Just (x, xs)
        | otherwise = Nothing

char :: Char -> Parser Char
char c = satisfy (== c)

posInt :: Integer -> Parser Integer
posInt x = Parser f
  where
    f xs
        | null ns = Nothing
        | otherwise = Just (read ns, rest)
        where (ns, rest) = span isDigit xs

instance Functor Parser where
    fmap f p = Parser g
      where g x = fmap (first f) $ (runParser p) x
            first z (a, b) = (z a, b)

instance Applicative Parser where
    pure a = Parser (\x -> Just (a, x))
    fp <*> p = Parser $ f fp p
      where 
        f p1 p2 s = case (runParser p1) s of
          Nothing -> Nothing
          Just (f', s') -> (runParser (f' <$> p2)) s'

instance Alternative Parser where
    empty = Parser $ const Nothing
    p1 <|> p2 = Parser $ \s -> runParser p1 s <|> runParser p2 s

zeroOrMore :: Parser a -> Parser [a]
zeroOrMore p = oneOrMore p <|> pure []

oneOrMore :: Parser a -> Parser [a]
oneOrMore p = liftA2 (:) p $ zeroOrMore p

spaces :: Parser String
spaces = zeroOrMore (satisfy isSpace)

ident :: Parser String
ident = liftA2 (:) (satisfy isAlpha)
