module AlgorithmW where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Control.Monad.Except
import Control.Monad.Trans.Except
import Control.Monad.Reader
import Control.Monad.State
import qualified Text.PrettyPrint as PP


data Exp = Var String 
         | App Exp Exp
         | Abs String Exp
         | Let String Exp Exp
         | LInt Integer
         | LBool Bool
         | If
         | Plus
         | Minus
         | Eq 
         | Y 
         | PrR
         | PrL
         | InL
         | InR
         | Pair Exp Exp
         | Case 
         deriving (Eq, Ord)

data Type = TVar String
          | Int
          | Bool
          | Fun Type Type
          | And Type Type
          | Or Type Type
          deriving (Eq, Ord)

data TypeScheme = Scheme [String] Type

type Subst = Map.Map String Type

newtype TypeEnv = TypeEnv (Map.Map String TypeScheme)

nullSubst :: Subst
nullSubst = Map.empty

composeSubst :: Subst -> Subst -> Subst
composeSubst s2 s1 = (Map.map (apply s2) s1) `Map.union` s2


class Types a where
    ftv   :: a -> Set.Set String
    apply :: Subst -> a -> a

instance Types Type where
    ftv (TVar x)  = Set.singleton x
    ftv Int       = Set.empty
    ftv Bool      = Set.empty
    ftv (Fun x y) = (ftv x) `Set.union` (ftv y) 
    ftv (And x y) = (ftv x) `Set.union` (ftv y) 
    ftv (Or x y)  = (ftv x) `Set.union` (ftv y) 
    apply s (TVar x)  = case Map.lookup x s of
                          Nothing -> (TVar x)
                          Just t  -> t
    apply s (Fun x y) = Fun (apply s x) (apply s y)
    apply s (And x y) = And (apply s x) (apply s y)
    apply s (Or x y)  = Or (apply s x) (apply s y)
    apply s t         = t

instance Types TypeScheme where
    ftv (Scheme l t) = Set.difference (ftv t) (Set.fromList l)
    apply s (Scheme l t) = Scheme l (apply (foldr Map.delete s l) t)


instance Types a => Types [a] where
    ftv l = foldr Set.union (Set.empty) (map ftv l)
    apply = map . apply

remove :: TypeEnv -> String -> TypeEnv
remove (TypeEnv env) x = TypeEnv $ Map.delete x env

instance Types TypeEnv where
    ftv (TypeEnv m) = ftv $ Map.elems m
    apply s (TypeEnv m) = TypeEnv $ Map.map (apply s) m

generalize :: TypeEnv -> Type -> TypeScheme
generalize env t = Scheme vars t
    where
        vars = Set.toList $ (ftv t) `Set.difference` (ftv env)

data TIEnv = TIEnv {}
data TIState = TIState {tiSupply :: Int, tiSubst :: Subst}
type TI a = ExceptT String (ReaderT TIEnv (StateT TIState IO)) a

runTI :: TI a -> IO (Either String a, TIState)
runTI t = do
    (res, st) <- runStateT (runReaderT (runExceptT t) initTIEnv) initTIState
    return (res, st)
        where 
            initTIEnv = TIEnv {}
            initTIState = TIState 0 Map.empty

newTVar :: String  -> TI Type
newTVar prefix = do
    s <- get
    put s{tiSupply = tiSupply s + 1}
    return (TVar (prefix ++ show (tiSupply s)))

instantiate :: TypeScheme -> TI Type
instantiate (Scheme vars t) = do 
    nvars <- mapM (\ _ -> newTVar "a") vars
    let s = Map.fromList (zip vars nvars)
    return $ apply s t

mgu :: Type -> Type -> TI Subst
mgu (Fun l r) (Fun l' r') = mgu' l r l' r'
mgu (And l r) (And l' r') = mgu' l r l' r'
mgu (Or l r) (Or l' r') = mgu' l r l' r'
mgu (TVar u) t = varBind u t
mgu t (TVar u) = varBind u t
mgu Int Int = return nullSubst
mgu Bool Bool = return nullSubst
mgu t1 t2 = throwE $ "types do not unify: " ++ show t1 ++ 
                               " and " ++ show t2 
mgu' :: Type -> Type -> Type -> Type -> TI Subst
mgu' l r l' r' = do
    s1 <- mgu l l'
    s2 <- mgu (apply s1 r) (apply s1 r')
    return (s1 `composeSubst` s2)

varBind :: String -> Type -> TI Subst
varBind u (TVar x) 
    | u == x = return nullSubst
    | otherwise = return (Map.singleton u (TVar x))
varBind u t
    | u `Set.member` (ftv t) = throwE $ "occur check: " ++
                               u ++ " and " -- ++ show t todo
    | otherwise = return (Map.singleton u t)

type Context = Map.Map String Type

algoW :: TypeEnv -> Exp -> TI (Subst, Type, Context)
algoW (TypeEnv env) (Var v) = 
    case Map.lookup v env of
      Nothing -> do t <- newTVar "x"
                    return (nullSubst, t, Map.singleton v t)
      Just x  -> do t <- instantiate x
                    return (nullSubst, t, Map.empty)
algoW _ (LInt _) = return (nullSubst, Int, Map.empty)
algoW _ (LBool _) = return (nullSubst, Bool, Map.empty)
algoW  env (Abs l e) = do
    b <- newTVar "a"
    let (TypeEnv env') = remove env l
        env'' = TypeEnv (env' `Map.union` (Map.singleton l (Scheme [] b)))
    (s1, t1, c1) <- algoW env'' e
    let b' = apply s1 b
    return (s1, Fun b' t1, c1)
algoW env (App e1 e2) = do
    b <- newTVar "a"
    (s1, t1, c1) <- algoW env e1
    let (TypeEnv env') = (apply s1 env)
    let env'' = TypeEnv $ env' `Map.union` 
            (Map.foldrWithKey (\k v r -> Map.insert k (Scheme [] v) r) Map.empty c1)
    (s2, t2, c2) <- algoW (apply s1 env'') e2
    s3 <- mgu (apply s2 t1) (Fun t2 b)
    let s = s3 `composeSubst` s2 `composeSubst` s1
        c = Map.map (apply s3) (Map.union c1 c2)
    return (s, apply s3 b, c)
algoW env (Let x i e) = do
    (s1, t1, c1) <- algoW env i 
    let (TypeEnv env') = remove env x
        (TypeEnv env'') = TypeEnv $ (Map.insert x (generalize (apply s1 env) t1) env')
        env''' = TypeEnv $ env'' `Map.union` 
            (Map.foldrWithKey (\k v r -> Map.insert k (Scheme [] v) r) Map.empty c1)
    (s2, t2, c2) <- algoW (apply s1 env''') e
    let c = Map.union c1 c2
    return (s2 `composeSubst` s1, t2, c)
algoW _ If = do
    a <- newTVar "a"
    return (nullSubst, Fun Bool (Fun a (Fun a a)), Map.empty)
algoW _ Plus = return (nullSubst, Fun Int (Fun Int Int), Map.empty)
algoW _ Minus = return (nullSubst, Fun Int (Fun Int Int), Map.empty)
algoW _ Eq = return (nullSubst, Fun Int (Fun Int Bool), Map.empty)
algoW env@(TypeEnv en) (Pair p1 p2) = do
    (s1, t1, c1) <- algoW env p1
    let env' = TypeEnv $ en `Map.union` 
            (Map.foldrWithKey (\k v r-> Map.insert k (Scheme [] v) r) Map.empty c1)
    (s2, t2, c2) <- algoW (apply s1 env') p2
    let c = Map.union c1 c2
    return (s2 `composeSubst` s1, And (apply s2 t1) t2, c)
algoW _ PrL = do
    a <- newTVar "b"
    b <- newTVar "c"
    return (nullSubst, Fun (And a b) a, Map.empty)
algoW _ PrR = do
    a <- newTVar "b"
    b <- newTVar "c"
    return (nullSubst, Fun (And a b) b, Map.empty)
algoW _ Case = do
    a <- newTVar "a"
    b <- newTVar "b"
    c <- newTVar "c"
    return (nullSubst, Fun (Or a b) (Fun (Fun a c) (Fun (Fun b c) c)), Map.empty)
algoW _ Y = do
    a <- newTVar "v"
    return (nullSubst, Fun (Fun a a) a, Map.empty)
algoW _ InL = do
    a <- newTVar "x"
    b <- newTVar "y"
    return (nullSubst, Fun a (Or a b), Map.empty)
algoW _ InR = do
    a <- newTVar "x"
    b <- newTVar "y"
    return (nullSubst, Fun b (Or a b), Map.empty)

findType :: Map.Map String TypeScheme -> Exp -> TI (Subst, Type, Context)
findType env e = do
    (s, t, c) <- algoW (TypeEnv env) e
    return (s, apply s t, c)

----------------------------------------------------------------------



instance Show Type where
    showsPrec _ = shows . printType

printType :: Type -> PP.Doc
printType (TVar x) = PP.text x
printType Int = PP.text "Int"
printType Bool = PP.text "Bool"
printType (Fun x y) = printParentT x PP.<+> PP.text "->" PP.<+> printType y
printType (And x y) = printParentT x PP.<+> PP.text "&" PP.<+> (printParentT y)
printType (Or x y) = printParentT x PP.<+> PP.text "|" PP.<+> (printParentT y)

printParentT :: Type -> PP.Doc
printParentT x = case x of
  Fun _ _ -> PP.parens (printType x)
  And _ _ -> PP.parens (printType x)
  Or _ _  -> PP.parens (printType x)
  _       -> printType x

instance Show Exp where
    showsPrec _ = shows . printExp

printExp :: Exp -> PP.Doc
printExp (Var n) = PP.text n
printExp (App e1 e2) = printExp e1 PP.<+> printParentE e2
printExp (Abs v e) = PP.char '\\' PP.<+> (PP.text v) PP.<+>
                     PP.text "->" PP.<+> printExp e
printExp (Let x i e) = PP.text "let" PP.<+> (PP.text x) PP.<+>
                       PP.text "=" PP.<+> printExp i PP.<+> PP.text "in"
                       PP.$$ PP.nest 2 (printExp e)
printExp (LInt x) = PP.integer x
printExp (LBool x) = PP.text $ if x then "True" else "False"
printExp If = PP.text "If"
printExp Plus = PP.text "Plus"
printExp Minus = PP.text "Minus"
printExp Eq = PP.text "Eq"
printExp Y = PP.text "Y"
printExp PrR = PP.text "PrR"
printExp PrL = PP.text "PrL"
printExp InL = PP.text "InL"
printExp InR = PP.text "InR"
printExp (Pair p1 p2) = PP.char '<' PP.<+> printExp p1 PP.<+> printExp p2 
                        PP.<+> PP.char '>'
printExp Case = PP.text "Case"

printParentE :: Exp -> PP.Doc
printParentE e = case e of
  Abs _ _   -> PP.parens $ printExp e
  App _ _   -> PP.parens $ printExp e
  Let _ _ _ -> PP.parens $ printExp e
  _         -> printExp e

instance Show TypeScheme where
    showsPrec _ = shows . printScheme

printScheme :: TypeScheme -> PP.Doc
printScheme (Scheme vars t) = PP.text "All" PP.<+> 
                              PP.hcat 
                               (PP.punctuate PP.comma (map PP.text vars))
                              PP.<> PP.text "." PP.<+> printType t
