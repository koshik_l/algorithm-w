module Printing where

import qualified Text.PrettyPrint as PP
import AlgorithmW


instance Show Type where
    showsPrec _ x = shows (printType x)

printType :: Type -> PP.Doc
printType (TVar x) = PP.text x
printType Int = PP.text "Int"
printType Bool = PP.text "Bool"
printType (Fun x y) = printType x PP.<+> PP.text "->" PP.<+> printParentT y
printType (And x y) = printParentT x PP.<+> PP.text "&" PP.<+> (printParentT y)
printType (Or x y) = printParentT x PP.<+> PP.text "|" PP.<+> (printParentT y)

printParentT :: Type -> PP.Doc
printParentT x = case x of
  Fun _ _ -> PP.parens (printType x)
  And _ _ -> PP.parens (printType x)
  Or _ _  -> PP.parens (printType x)
  _       -> printType x

instance Show Exp
