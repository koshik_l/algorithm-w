import AlgorithmW
--import Printing
import Parser
import qualified Data.Map as Map

main :: IO ()
main = do
    expr <- readFile "in.txt"
    let e = (exp2Exp . parse . lexer) expr 
    let f k v r = r ++ "\n" ++ (show k) ++ " : " ++ (show v) 
    (res, _) <- runTI (findType Map.empty e)
    case res of
      Left err -> putStrLn $ "Лямбда-выражение не имеет типа."
      Right (_, t, c) -> putStrLn $ (show t)  ++ (Map.foldrWithKey f "" c) 
