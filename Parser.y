{
module Parser where
import Data.Char
import AlgorithmW
}

%name parse
%tokentype { Token }
%error { parseError }

%token 
      let      { TokenLet }
      in       { TokenIn }
      int      { TokenInt $$ }
      var      { TokenVar $$ }
      f        { TokenF }
      t        { TokenT }
      if       { TokenIf }
      y        { TokenY }
      plus     { TokenPlus }
      minus    { TokenMinus }
      eq       { TokenEq }
      prr      { TokenPrR }
      prl      { TokenPrL }
      inl      { TokenInL }
      inr      { TokenInR }
      case     { TokenCase }
      '='      { TokenEqS }
      '('      { TokenLp }
      ')'      { TokenRp }
      '<'      { TokenQlp }
      '>'      { TokenQrp }
      '\\'     { TokenLam }
      ','      { TokenComma }
      '.'      { TokenDot }
%%

ExpP  : let var '=' ExpP in ExpP     { LetP $2 $4 $6 }
	  | AbsP                         { AbsE $1 }

AbsP  : '\\' var '.' AbsP            { AbsX $2 $4 }
	  | AppP                         { AppP $1 }

AppP  : AppP Term                   { AppX $1 $2 }
	  | Term                        { Term $1 }

Term  : '(' AbsP ')'                { AbsT $2 }
	  | Value                       { Value $1 }

Value : f                           { FalseP }
      | t                           { TrueP }
      | if                          { IfP }
      | y                           { YP }
      | plus                        { PlusP }
      | minus                       { MinusP }
      | eq                          { EqP }
      | '<' AbsP ',' AbsP '>'       { PairP $2 $4 }
      | prr                         { PrRP }
      | prl                         { PrLP }
      | inl                         { InLP }
      | inr                         { InRP }
      | case                        { CaseP }
      | var                         { VarP $1 }
      | int                         { LIntP $1 }

{
parseError :: [Token] -> a
parseError _ = error "Parse error"

data ExpP = LetP String ExpP ExpP
          | AbsE AbsP 

data AbsP = AbsX String AbsP
          | AppP AppP

data AppP = AppX AppP Term
          | Term Term

data Term = AbsT AbsP | Value Value

data Value = TrueP | FalseP | IfP | YP 
           | PlusP | MinusP | EqP | PairP AbsP AbsP
           | PrRP  | PrLP   | InLP| InRP
           | CaseP | VarP String   | LIntP Integer

exp2Exp :: ExpP -> Exp
exp2Exp (LetP v i e) = Let v (exp2Exp i) (exp2Exp e)
exp2Exp (AbsE a) = abs2Exp a

abs2Exp :: AbsP -> Exp
abs2Exp (AbsX v a) = Abs v (abs2Exp a)
abs2Exp (AppP a)   = app2Exp a

app2Exp :: AppP -> Exp
app2Exp (AppX a t) = App (app2Exp a) (term2Exp t)
app2Exp (Term t) = term2Exp t

term2Exp :: Term -> Exp
term2Exp (AbsT a) = abs2Exp a
term2Exp (Value v) = val2Exp v

val2Exp :: Value -> Exp
val2Exp TrueP = LBool True
val2Exp FalseP = LBool False
val2Exp IfP = If
val2Exp YP = Y
val2Exp PlusP = Plus
val2Exp MinusP = Minus
val2Exp EqP = Eq
val2Exp (PairP a1 a2) = Pair (abs2Exp a1) (abs2Exp a2)
val2Exp PrRP = PrR
val2Exp PrLP = PrL
val2Exp InLP = InL
val2Exp InRP = InR
val2Exp CaseP = Case
val2Exp (VarP v) = Var v
val2Exp (LIntP x) = LInt x



data Token
      = TokenLet
      | TokenIn 
      | TokenInt Integer
      | TokenVar String
      | TokenF 
      | TokenT 
      | TokenIf 
      | TokenY 
      | TokenPlus 
      | TokenMinus 
      | TokenEq 
      | TokenPrR 
      | TokenPrL 
      | TokenInL 
      | TokenInR 
      | TokenCase 
      | TokenEqS 
      | TokenLp 
      | TokenRp 
      | TokenQlp 
      | TokenQrp 
      | TokenLam 
      | TokenComma 
      | TokenDot 
  deriving Show

lexer :: String -> [Token]
lexer [] = []
lexer ('=':cs) = TokenEqS : lexer cs
lexer ('\\':cs) = TokenLam : lexer cs
lexer ('.':cs) = TokenDot : lexer cs
lexer (',':cs) = TokenComma : lexer cs
lexer ('<':cs) = TokenQlp : lexer cs
lexer ('>':cs) = TokenQrp : lexer cs
lexer ('(':cs) = TokenLp : lexer cs
lexer (')':cs) = TokenRp : lexer cs
lexer ('Y':cs) = TokenY : lexer cs
lexer ('F':cs) = TokenF : lexer cs
lexer ('T':cs) = TokenT : lexer cs
lexer a@('C':cs) = case nextWord a of 
                   ("Case", rest) -> TokenCase : lexer rest
                   _              -> error "Case"
lexer a@('I':cs) = case nextWord a of 
                   ("InR", r) -> TokenInR : lexer r
                   ("InL", r) -> TokenInL : lexer r
                   ("If", r)  -> TokenIf  : lexer r
                   _          -> error "InR/InL/If"
lexer a@('P':cs) = case nextWord a of
                   ("PrR", r) -> TokenPrR : lexer r
                   ("PrL", r) -> TokenPrL : lexer r
                   ("Plus", r) -> TokenPlus : lexer r
                   _          -> error "PrR/PrL/Plus"
lexer a@('E':cs) = case nextWord a of
                   ("Eq", r) -> TokenEq : lexer r
                   _           -> error "Eq"
lexer a@('M':cs) = case nextWord a of
                   ("Minus", r) -> TokenMinus : lexer r
                   _            -> error "Minus"
lexer a@('l':cs) = case nextWord a of
                   ("let", r) -> TokenLet : lexer r
                   _          -> lexId a
lexer a@('i':cs) = case nextWord a of
                   ("in", r) -> TokenIn : lexer r
                   _         -> lexId a
lexer a@(c:cs) 
      | isSpace c = lexer cs
      | isDigit c = lexNum a
      | isAlpha c = lexId a


nextWord = span isAlpha

lexNum x = TokenInt (read num) : lexer r
  where (num, r) = span isDigit x

lexId x = TokenVar v : lexer r
  where (v, r) = span isAlphaNum x


}
