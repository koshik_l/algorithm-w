{-# OPTIONS_GHC -w #-}
module Parser where
import Data.Char
import AlgorithmW
import Control.Applicative(Applicative(..))
import Control.Monad (ap)

-- parser produced by Happy Version 1.19.5

data HappyAbsSyn t4 t5 t6 t7 t8
	= HappyTerminal (Token)
	| HappyErrorToken Int
	| HappyAbsSyn4 t4
	| HappyAbsSyn5 t5
	| HappyAbsSyn6 t6
	| HappyAbsSyn7 t7
	| HappyAbsSyn8 t8

action_0 (9) = happyShift action_2
action_0 (11) = happyShift action_8
action_0 (12) = happyShift action_9
action_0 (13) = happyShift action_10
action_0 (14) = happyShift action_11
action_0 (15) = happyShift action_12
action_0 (16) = happyShift action_13
action_0 (17) = happyShift action_14
action_0 (18) = happyShift action_15
action_0 (19) = happyShift action_16
action_0 (20) = happyShift action_17
action_0 (21) = happyShift action_18
action_0 (22) = happyShift action_19
action_0 (23) = happyShift action_20
action_0 (24) = happyShift action_21
action_0 (26) = happyShift action_22
action_0 (28) = happyShift action_23
action_0 (30) = happyShift action_24
action_0 (4) = happyGoto action_3
action_0 (5) = happyGoto action_4
action_0 (6) = happyGoto action_5
action_0 (7) = happyGoto action_6
action_0 (8) = happyGoto action_7
action_0 _ = happyFail

action_1 (9) = happyShift action_2
action_1 _ = happyFail

action_2 (12) = happyShift action_29
action_2 _ = happyFail

action_3 (33) = happyAccept
action_3 _ = happyFail

action_4 _ = happyReduce_2

action_5 (11) = happyShift action_8
action_5 (12) = happyShift action_9
action_5 (13) = happyShift action_10
action_5 (14) = happyShift action_11
action_5 (15) = happyShift action_12
action_5 (16) = happyShift action_13
action_5 (17) = happyShift action_14
action_5 (18) = happyShift action_15
action_5 (19) = happyShift action_16
action_5 (20) = happyShift action_17
action_5 (21) = happyShift action_18
action_5 (22) = happyShift action_19
action_5 (23) = happyShift action_20
action_5 (24) = happyShift action_21
action_5 (26) = happyShift action_22
action_5 (28) = happyShift action_23
action_5 (7) = happyGoto action_28
action_5 (8) = happyGoto action_7
action_5 _ = happyReduce_4

action_6 _ = happyReduce_6

action_7 _ = happyReduce_8

action_8 _ = happyReduce_23

action_9 _ = happyReduce_22

action_10 _ = happyReduce_9

action_11 _ = happyReduce_10

action_12 _ = happyReduce_11

action_13 _ = happyReduce_12

action_14 _ = happyReduce_13

action_15 _ = happyReduce_14

action_16 _ = happyReduce_15

action_17 _ = happyReduce_17

action_18 _ = happyReduce_18

action_19 _ = happyReduce_19

action_20 _ = happyReduce_20

action_21 _ = happyReduce_21

action_22 (11) = happyShift action_8
action_22 (12) = happyShift action_9
action_22 (13) = happyShift action_10
action_22 (14) = happyShift action_11
action_22 (15) = happyShift action_12
action_22 (16) = happyShift action_13
action_22 (17) = happyShift action_14
action_22 (18) = happyShift action_15
action_22 (19) = happyShift action_16
action_22 (20) = happyShift action_17
action_22 (21) = happyShift action_18
action_22 (22) = happyShift action_19
action_22 (23) = happyShift action_20
action_22 (24) = happyShift action_21
action_22 (26) = happyShift action_22
action_22 (28) = happyShift action_23
action_22 (30) = happyShift action_24
action_22 (5) = happyGoto action_27
action_22 (6) = happyGoto action_5
action_22 (7) = happyGoto action_6
action_22 (8) = happyGoto action_7
action_22 _ = happyFail

action_23 (11) = happyShift action_8
action_23 (12) = happyShift action_9
action_23 (13) = happyShift action_10
action_23 (14) = happyShift action_11
action_23 (15) = happyShift action_12
action_23 (16) = happyShift action_13
action_23 (17) = happyShift action_14
action_23 (18) = happyShift action_15
action_23 (19) = happyShift action_16
action_23 (20) = happyShift action_17
action_23 (21) = happyShift action_18
action_23 (22) = happyShift action_19
action_23 (23) = happyShift action_20
action_23 (24) = happyShift action_21
action_23 (26) = happyShift action_22
action_23 (28) = happyShift action_23
action_23 (30) = happyShift action_24
action_23 (5) = happyGoto action_26
action_23 (6) = happyGoto action_5
action_23 (7) = happyGoto action_6
action_23 (8) = happyGoto action_7
action_23 _ = happyFail

action_24 (12) = happyShift action_25
action_24 _ = happyFail

action_25 (32) = happyShift action_33
action_25 _ = happyFail

action_26 (31) = happyShift action_32
action_26 _ = happyFail

action_27 (27) = happyShift action_31
action_27 _ = happyFail

action_28 _ = happyReduce_5

action_29 (25) = happyShift action_30
action_29 _ = happyFail

action_30 (9) = happyShift action_2
action_30 (11) = happyShift action_8
action_30 (12) = happyShift action_9
action_30 (13) = happyShift action_10
action_30 (14) = happyShift action_11
action_30 (15) = happyShift action_12
action_30 (16) = happyShift action_13
action_30 (17) = happyShift action_14
action_30 (18) = happyShift action_15
action_30 (19) = happyShift action_16
action_30 (20) = happyShift action_17
action_30 (21) = happyShift action_18
action_30 (22) = happyShift action_19
action_30 (23) = happyShift action_20
action_30 (24) = happyShift action_21
action_30 (26) = happyShift action_22
action_30 (28) = happyShift action_23
action_30 (30) = happyShift action_24
action_30 (4) = happyGoto action_36
action_30 (5) = happyGoto action_4
action_30 (6) = happyGoto action_5
action_30 (7) = happyGoto action_6
action_30 (8) = happyGoto action_7
action_30 _ = happyFail

action_31 _ = happyReduce_7

action_32 (11) = happyShift action_8
action_32 (12) = happyShift action_9
action_32 (13) = happyShift action_10
action_32 (14) = happyShift action_11
action_32 (15) = happyShift action_12
action_32 (16) = happyShift action_13
action_32 (17) = happyShift action_14
action_32 (18) = happyShift action_15
action_32 (19) = happyShift action_16
action_32 (20) = happyShift action_17
action_32 (21) = happyShift action_18
action_32 (22) = happyShift action_19
action_32 (23) = happyShift action_20
action_32 (24) = happyShift action_21
action_32 (26) = happyShift action_22
action_32 (28) = happyShift action_23
action_32 (30) = happyShift action_24
action_32 (5) = happyGoto action_35
action_32 (6) = happyGoto action_5
action_32 (7) = happyGoto action_6
action_32 (8) = happyGoto action_7
action_32 _ = happyFail

action_33 (11) = happyShift action_8
action_33 (12) = happyShift action_9
action_33 (13) = happyShift action_10
action_33 (14) = happyShift action_11
action_33 (15) = happyShift action_12
action_33 (16) = happyShift action_13
action_33 (17) = happyShift action_14
action_33 (18) = happyShift action_15
action_33 (19) = happyShift action_16
action_33 (20) = happyShift action_17
action_33 (21) = happyShift action_18
action_33 (22) = happyShift action_19
action_33 (23) = happyShift action_20
action_33 (24) = happyShift action_21
action_33 (26) = happyShift action_22
action_33 (28) = happyShift action_23
action_33 (30) = happyShift action_24
action_33 (5) = happyGoto action_34
action_33 (6) = happyGoto action_5
action_33 (7) = happyGoto action_6
action_33 (8) = happyGoto action_7
action_33 _ = happyFail

action_34 _ = happyReduce_3

action_35 (29) = happyShift action_38
action_35 _ = happyFail

action_36 (10) = happyShift action_37
action_36 _ = happyFail

action_37 (9) = happyShift action_2
action_37 (11) = happyShift action_8
action_37 (12) = happyShift action_9
action_37 (13) = happyShift action_10
action_37 (14) = happyShift action_11
action_37 (15) = happyShift action_12
action_37 (16) = happyShift action_13
action_37 (17) = happyShift action_14
action_37 (18) = happyShift action_15
action_37 (19) = happyShift action_16
action_37 (20) = happyShift action_17
action_37 (21) = happyShift action_18
action_37 (22) = happyShift action_19
action_37 (23) = happyShift action_20
action_37 (24) = happyShift action_21
action_37 (26) = happyShift action_22
action_37 (28) = happyShift action_23
action_37 (30) = happyShift action_24
action_37 (4) = happyGoto action_39
action_37 (5) = happyGoto action_4
action_37 (6) = happyGoto action_5
action_37 (7) = happyGoto action_6
action_37 (8) = happyGoto action_7
action_37 _ = happyFail

action_38 _ = happyReduce_16

action_39 _ = happyReduce_1

happyReduce_1 = happyReduce 6 4 happyReduction_1
happyReduction_1 ((HappyAbsSyn4  happy_var_6) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn4  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TokenVar happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn4
		 (LetP happy_var_2 happy_var_4 happy_var_6
	) `HappyStk` happyRest

happyReduce_2 = happySpecReduce_1  4 happyReduction_2
happyReduction_2 (HappyAbsSyn5  happy_var_1)
	 =  HappyAbsSyn4
		 (AbsE happy_var_1
	)
happyReduction_2 _  = notHappyAtAll 

happyReduce_3 = happyReduce 4 5 happyReduction_3
happyReduction_3 ((HappyAbsSyn5  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TokenVar happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn5
		 (AbsX happy_var_2 happy_var_4
	) `HappyStk` happyRest

happyReduce_4 = happySpecReduce_1  5 happyReduction_4
happyReduction_4 (HappyAbsSyn6  happy_var_1)
	 =  HappyAbsSyn5
		 (AppP happy_var_1
	)
happyReduction_4 _  = notHappyAtAll 

happyReduce_5 = happySpecReduce_2  6 happyReduction_5
happyReduction_5 (HappyAbsSyn7  happy_var_2)
	(HappyAbsSyn6  happy_var_1)
	 =  HappyAbsSyn6
		 (AppX happy_var_1 happy_var_2
	)
happyReduction_5 _ _  = notHappyAtAll 

happyReduce_6 = happySpecReduce_1  6 happyReduction_6
happyReduction_6 (HappyAbsSyn7  happy_var_1)
	 =  HappyAbsSyn6
		 (Term happy_var_1
	)
happyReduction_6 _  = notHappyAtAll 

happyReduce_7 = happySpecReduce_3  7 happyReduction_7
happyReduction_7 _
	(HappyAbsSyn5  happy_var_2)
	_
	 =  HappyAbsSyn7
		 (AbsT happy_var_2
	)
happyReduction_7 _ _ _  = notHappyAtAll 

happyReduce_8 = happySpecReduce_1  7 happyReduction_8
happyReduction_8 (HappyAbsSyn8  happy_var_1)
	 =  HappyAbsSyn7
		 (Value happy_var_1
	)
happyReduction_8 _  = notHappyAtAll 

happyReduce_9 = happySpecReduce_1  8 happyReduction_9
happyReduction_9 _
	 =  HappyAbsSyn8
		 (FalseP
	)

happyReduce_10 = happySpecReduce_1  8 happyReduction_10
happyReduction_10 _
	 =  HappyAbsSyn8
		 (TrueP
	)

happyReduce_11 = happySpecReduce_1  8 happyReduction_11
happyReduction_11 _
	 =  HappyAbsSyn8
		 (IfP
	)

happyReduce_12 = happySpecReduce_1  8 happyReduction_12
happyReduction_12 _
	 =  HappyAbsSyn8
		 (YP
	)

happyReduce_13 = happySpecReduce_1  8 happyReduction_13
happyReduction_13 _
	 =  HappyAbsSyn8
		 (PlusP
	)

happyReduce_14 = happySpecReduce_1  8 happyReduction_14
happyReduction_14 _
	 =  HappyAbsSyn8
		 (MinusP
	)

happyReduce_15 = happySpecReduce_1  8 happyReduction_15
happyReduction_15 _
	 =  HappyAbsSyn8
		 (EqP
	)

happyReduce_16 = happyReduce 5 8 happyReduction_16
happyReduction_16 (_ `HappyStk`
	(HappyAbsSyn5  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn5  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn8
		 (PairP happy_var_2 happy_var_4
	) `HappyStk` happyRest

happyReduce_17 = happySpecReduce_1  8 happyReduction_17
happyReduction_17 _
	 =  HappyAbsSyn8
		 (PrRP
	)

happyReduce_18 = happySpecReduce_1  8 happyReduction_18
happyReduction_18 _
	 =  HappyAbsSyn8
		 (PrLP
	)

happyReduce_19 = happySpecReduce_1  8 happyReduction_19
happyReduction_19 _
	 =  HappyAbsSyn8
		 (InLP
	)

happyReduce_20 = happySpecReduce_1  8 happyReduction_20
happyReduction_20 _
	 =  HappyAbsSyn8
		 (InRP
	)

happyReduce_21 = happySpecReduce_1  8 happyReduction_21
happyReduction_21 _
	 =  HappyAbsSyn8
		 (CaseP
	)

happyReduce_22 = happySpecReduce_1  8 happyReduction_22
happyReduction_22 (HappyTerminal (TokenVar happy_var_1))
	 =  HappyAbsSyn8
		 (VarP happy_var_1
	)
happyReduction_22 _  = notHappyAtAll 

happyReduce_23 = happySpecReduce_1  8 happyReduction_23
happyReduction_23 (HappyTerminal (TokenInt happy_var_1))
	 =  HappyAbsSyn8
		 (LIntP happy_var_1
	)
happyReduction_23 _  = notHappyAtAll 

happyNewToken action sts stk [] =
	action 33 33 notHappyAtAll (HappyState action) sts stk []

happyNewToken action sts stk (tk:tks) =
	let cont i = action i i tk (HappyState action) sts stk tks in
	case tk of {
	TokenLet -> cont 9;
	TokenIn -> cont 10;
	TokenInt happy_dollar_dollar -> cont 11;
	TokenVar happy_dollar_dollar -> cont 12;
	TokenF -> cont 13;
	TokenT -> cont 14;
	TokenIf -> cont 15;
	TokenY -> cont 16;
	TokenPlus -> cont 17;
	TokenMinus -> cont 18;
	TokenEq -> cont 19;
	TokenPrR -> cont 20;
	TokenPrL -> cont 21;
	TokenInL -> cont 22;
	TokenInR -> cont 23;
	TokenCase -> cont 24;
	TokenEqS -> cont 25;
	TokenLp -> cont 26;
	TokenRp -> cont 27;
	TokenQlp -> cont 28;
	TokenQrp -> cont 29;
	TokenLam -> cont 30;
	TokenComma -> cont 31;
	TokenDot -> cont 32;
	_ -> happyError' (tk:tks)
	}

happyError_ 33 tk tks = happyError' tks
happyError_ _ tk tks = happyError' (tk:tks)

newtype HappyIdentity a = HappyIdentity a
happyIdentity = HappyIdentity
happyRunIdentity (HappyIdentity a) = a

instance Functor HappyIdentity where
    fmap f (HappyIdentity a) = HappyIdentity (f a)

instance Applicative HappyIdentity where
    pure  = return
    (<*>) = ap
instance Monad HappyIdentity where
    return = HappyIdentity
    (HappyIdentity p) >>= q = q p

happyThen :: () => HappyIdentity a -> (a -> HappyIdentity b) -> HappyIdentity b
happyThen = (>>=)
happyReturn :: () => a -> HappyIdentity a
happyReturn = (return)
happyThen1 m k tks = (>>=) m (\a -> k a tks)
happyReturn1 :: () => a -> b -> HappyIdentity a
happyReturn1 = \a tks -> (return) a
happyError' :: () => [(Token)] -> HappyIdentity a
happyError' = HappyIdentity . parseError

parse tks = happyRunIdentity happySomeParser where
  happySomeParser = happyThen (happyParse action_0 tks) (\x -> case x of {HappyAbsSyn4 z -> happyReturn z; _other -> notHappyAtAll })

happySeq = happyDontSeq


parseError :: [Token] -> a
parseError _ = error "Parse error"

data ExpP = LetP String ExpP ExpP
          | AbsE AbsP 

data AbsP = AbsX String AbsP
          | AppP AppP

data AppP = AppX AppP Term
          | Term Term

data Term = AbsT AbsP | Value Value

data Value = TrueP | FalseP | IfP | YP 
           | PlusP | MinusP | EqP | PairP AbsP AbsP
           | PrRP  | PrLP   | InLP| InRP
           | CaseP | VarP String   | LIntP Integer

exp2Exp :: ExpP -> Exp
exp2Exp (LetP v i e) = Let v (exp2Exp i) (exp2Exp e)
exp2Exp (AbsE a) = abs2Exp a

abs2Exp :: AbsP -> Exp
abs2Exp (AbsX v a) = Abs v (abs2Exp a)
abs2Exp (AppP a)   = app2Exp a

app2Exp :: AppP -> Exp
app2Exp (AppX a t) = App (app2Exp a) (term2Exp t)
app2Exp (Term t) = term2Exp t

term2Exp :: Term -> Exp
term2Exp (AbsT a) = abs2Exp a
term2Exp (Value v) = val2Exp v

val2Exp :: Value -> Exp
val2Exp TrueP = LBool True
val2Exp FalseP = LBool False
val2Exp IfP = If
val2Exp YP = Y
val2Exp PlusP = Plus
val2Exp MinusP = Minus
val2Exp EqP = Eq
val2Exp (PairP a1 a2) = Pair (abs2Exp a1) (abs2Exp a2)
val2Exp PrRP = PrR
val2Exp PrLP = PrL
val2Exp InLP = InL
val2Exp InRP = InR
val2Exp CaseP = Case
val2Exp (VarP v) = Var v
val2Exp (LIntP x) = LInt x



data Token
      = TokenLet
      | TokenIn 
      | TokenInt Integer
      | TokenVar String
      | TokenF 
      | TokenT 
      | TokenIf 
      | TokenY 
      | TokenPlus 
      | TokenMinus 
      | TokenEq 
      | TokenPrR 
      | TokenPrL 
      | TokenInL 
      | TokenInR 
      | TokenCase 
      | TokenEqS 
      | TokenLp 
      | TokenRp 
      | TokenQlp 
      | TokenQrp 
      | TokenLam 
      | TokenComma 
      | TokenDot 
  deriving Show

lexer :: String -> [Token]
lexer [] = []
lexer ('=':cs) = TokenEqS : lexer cs
lexer ('\\':cs) = TokenLam : lexer cs
lexer ('.':cs) = TokenDot : lexer cs
lexer (',':cs) = TokenComma : lexer cs
lexer ('<':cs) = TokenQlp : lexer cs
lexer ('>':cs) = TokenQrp : lexer cs
lexer ('(':cs) = TokenLp : lexer cs
lexer (')':cs) = TokenRp : lexer cs
lexer ('Y':cs) = TokenY : lexer cs
lexer ('F':cs) = TokenF : lexer cs
lexer ('T':cs) = TokenT : lexer cs
lexer a@('C':cs) = case nextWord a of 
                   ("Case", rest) -> TokenCase : lexer rest
                   _              -> error "Case"
lexer a@('I':cs) = case nextWord a of 
                   ("InR", r) -> TokenInR : lexer r
                   ("InL", r) -> TokenInL : lexer r
                   ("If", r)  -> TokenIf  : lexer r
                   _          -> error "InR/InL/If"
lexer a@('P':cs) = case nextWord a of
                   ("PrR", r) -> TokenPrR : lexer r
                   ("PrL", r) -> TokenPrL : lexer r
                   ("Plus", r) -> TokenPlus : lexer r
                   _          -> error "PrR/PrL/Plus"
lexer a@('E':cs) = case nextWord a of
                   ("Eq", r) -> TokenEq : lexer r
                   _           -> error "Eq"
lexer a@('M':cs) = case nextWord a of
                   ("Minus", r) -> TokenMinus : lexer r
                   _            -> error "Minus"
lexer a@('l':cs) = case nextWord a of
                   ("let", r) -> TokenLet : lexer r
                   _          -> lexId a
lexer a@('i':cs) = case nextWord a of
                   ("in", r) -> TokenIn : lexer r
                   _         -> lexId a
lexer a@(c:cs) 
      | isSpace c = lexer cs
      | isDigit c = lexNum a
      | isAlpha c = lexId a


nextWord = span isAlpha

lexNum x = TokenInt (read num) : lexer r
  where (num, r) = span isDigit x

lexId x = TokenVar v : lexer r
  where (v, r) = span isAlphaNum x
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "<built-in>" #-}
{-# LINE 1 "<command-line>" #-}
{-# LINE 8 "<command-line>" #-}
# 1 "/usr/include/stdc-predef.h" 1 3 4

# 17 "/usr/include/stdc-predef.h" 3 4















































{-# LINE 8 "<command-line>" #-}
{-# LINE 1 "/usr/lib/ghc-7.10.2/include/ghcversion.h" #-}

















{-# LINE 8 "<command-line>" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
-- Id: GenericTemplate.hs,v 1.26 2005/01/14 14:47:22 simonmar Exp 

{-# LINE 13 "templates/GenericTemplate.hs" #-}

{-# LINE 46 "templates/GenericTemplate.hs" #-}








{-# LINE 67 "templates/GenericTemplate.hs" #-}

{-# LINE 77 "templates/GenericTemplate.hs" #-}

{-# LINE 86 "templates/GenericTemplate.hs" #-}

infixr 9 `HappyStk`
data HappyStk a = HappyStk a (HappyStk a)

-----------------------------------------------------------------------------
-- starting the parse

happyParse start_state = happyNewToken start_state notHappyAtAll notHappyAtAll

-----------------------------------------------------------------------------
-- Accepting the parse

-- If the current token is (1), it means we've just accepted a partial
-- parse (a %partial parser).  We must ignore the saved token on the top of
-- the stack in this case.
happyAccept (1) tk st sts (_ `HappyStk` ans `HappyStk` _) =
        happyReturn1 ans
happyAccept j tk st sts (HappyStk ans _) = 
         (happyReturn1 ans)

-----------------------------------------------------------------------------
-- Arrays only: do the next action

{-# LINE 155 "templates/GenericTemplate.hs" #-}

-----------------------------------------------------------------------------
-- HappyState data type (not arrays)



newtype HappyState b c = HappyState
        (Int ->                    -- token number
         Int ->                    -- token number (yes, again)
         b ->                           -- token semantic value
         HappyState b c ->              -- current state
         [HappyState b c] ->            -- state stack
         c)



-----------------------------------------------------------------------------
-- Shifting a token

happyShift new_state (1) tk st sts stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--     trace "shifting the error token" $
     new_state i i tk (HappyState (new_state)) ((st):(sts)) (stk)

happyShift new_state i tk st sts stk =
     happyNewToken new_state ((st):(sts)) ((HappyTerminal (tk))`HappyStk`stk)

-- happyReduce is specialised for the common cases.

happySpecReduce_0 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_0 nt fn j tk st@((HappyState (action))) sts stk
     = action nt j tk st ((st):(sts)) (fn `HappyStk` stk)

happySpecReduce_1 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_1 nt fn j tk _ sts@(((st@(HappyState (action))):(_))) (v1`HappyStk`stk')
     = let r = fn v1 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_2 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_2 nt fn j tk _ ((_):(sts@(((st@(HappyState (action))):(_))))) (v1`HappyStk`v2`HappyStk`stk')
     = let r = fn v1 v2 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_3 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_3 nt fn j tk _ ((_):(((_):(sts@(((st@(HappyState (action))):(_))))))) (v1`HappyStk`v2`HappyStk`v3`HappyStk`stk')
     = let r = fn v1 v2 v3 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happyReduce k i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyReduce k nt fn j tk st sts stk
     = case happyDrop (k - ((1) :: Int)) sts of
         sts1@(((st1@(HappyState (action))):(_))) ->
                let r = fn stk in  -- it doesn't hurt to always seq here...
                happyDoSeq r (action nt j tk st1 sts1 r)

happyMonadReduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonadReduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
          let drop_stk = happyDropStk k stk in
          happyThen1 (fn stk tk) (\r -> action nt j tk st1 sts1 (r `HappyStk` drop_stk))

happyMonad2Reduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonad2Reduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
         let drop_stk = happyDropStk k stk





             new_state = action

          in
          happyThen1 (fn stk tk) (\r -> happyNewToken new_state sts1 (r `HappyStk` drop_stk))

happyDrop (0) l = l
happyDrop n ((_):(t)) = happyDrop (n - ((1) :: Int)) t

happyDropStk (0) l = l
happyDropStk n (x `HappyStk` xs) = happyDropStk (n - ((1)::Int)) xs

-----------------------------------------------------------------------------
-- Moving to a new state after a reduction

{-# LINE 256 "templates/GenericTemplate.hs" #-}
happyGoto action j tk st = action j j tk (HappyState action)


-----------------------------------------------------------------------------
-- Error recovery ((1) is the error token)

-- parse error if we are in recovery and we fail again
happyFail (1) tk old_st _ stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--      trace "failing" $ 
        happyError_ i tk

{-  We don't need state discarding for our restricted implementation of
    "error".  In fact, it can cause some bogus parses, so I've disabled it
    for now --SDM

-- discard a state
happyFail  (1) tk old_st (((HappyState (action))):(sts)) 
                                                (saved_tok `HappyStk` _ `HappyStk` stk) =
--      trace ("discarding state, depth " ++ show (length stk))  $
        action (1) (1) tk (HappyState (action)) sts ((saved_tok`HappyStk`stk))
-}

-- Enter error recovery: generate an error token,
--                       save the old token and carry on.
happyFail  i tk (HappyState (action)) sts stk =
--      trace "entering error recovery" $
        action (1) (1) tk (HappyState (action)) sts ( (HappyErrorToken (i)) `HappyStk` stk)

-- Internal happy errors:

notHappyAtAll :: a
notHappyAtAll = error "Internal Happy error\n"

-----------------------------------------------------------------------------
-- Hack to get the typechecker to accept our action functions







-----------------------------------------------------------------------------
-- Seq-ing.  If the --strict flag is given, then Happy emits 
--      happySeq = happyDoSeq
-- otherwise it emits
--      happySeq = happyDontSeq

happyDoSeq, happyDontSeq :: a -> b -> b
happyDoSeq   a b = a `seq` b
happyDontSeq a b = b

-----------------------------------------------------------------------------
-- Don't inline any functions from the template.  GHC has a nasty habit
-- of deciding to inline happyGoto everywhere, which increases the size of
-- the generated parser quite a bit.

{-# LINE 322 "templates/GenericTemplate.hs" #-}
{-# NOINLINE happyShift #-}
{-# NOINLINE happySpecReduce_0 #-}
{-# NOINLINE happySpecReduce_1 #-}
{-# NOINLINE happySpecReduce_2 #-}
{-# NOINLINE happySpecReduce_3 #-}
{-# NOINLINE happyReduce #-}
{-# NOINLINE happyMonadReduce #-}
{-# NOINLINE happyGoto #-}
{-# NOINLINE happyFail #-}

-- end of Happy Template.
